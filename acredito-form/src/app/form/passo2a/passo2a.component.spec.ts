import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Passo2aComponent } from './passo2a.component';

describe('Passo2aComponent', () => {
  let component: Passo2aComponent;
  let fixture: ComponentFixture<Passo2aComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Passo2aComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Passo2aComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
