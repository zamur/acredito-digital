import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Passo3aComponent } from './passo3a.component';

describe('Passo3aComponent', () => {
  let component: Passo3aComponent;
  let fixture: ComponentFixture<Passo3aComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Passo3aComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Passo3aComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
