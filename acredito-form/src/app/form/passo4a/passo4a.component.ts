import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Passo4aModalComponent } from './passo4a-modal/passo4a-modal.component';

@Component({
  selector: 'app-passo4a',
  templateUrl: './passo4a.component.html',
  styleUrls: ['./passo4a.component.scss']
})
export class Passo4aComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }

  open() {
    const modalRef = this.modalService.open(Passo4aModalComponent);
    // modalRef.componentInstance.name = 'World';
  }
}
