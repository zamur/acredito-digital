import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Passo4aComponent } from './passo4a.component';

describe('Passo4aComponent', () => {
  let component: Passo4aComponent;
  let fixture: ComponentFixture<Passo4aComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Passo4aComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Passo4aComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
