import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-passo4a-modal',
  templateUrl: './passo4a-modal.component.html',
  styleUrls: ['./passo4a-modal.component.scss']
})
export class Passo4aModalComponent {

  constructor(public activeModal: NgbActiveModal) { }

}
