import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Passo4aModalComponent } from './passo4a-modal.component';

describe('Passo4aModalComponent', () => {
  let component: Passo4aModalComponent;
  let fixture: ComponentFixture<Passo4aModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Passo4aModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Passo4aModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
