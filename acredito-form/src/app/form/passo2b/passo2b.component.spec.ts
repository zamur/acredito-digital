import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Passo2bComponent } from './passo2b.component';

describe('Passo2bComponent', () => {
  let component: Passo2bComponent;
  let fixture: ComponentFixture<Passo2bComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Passo2bComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Passo2bComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
