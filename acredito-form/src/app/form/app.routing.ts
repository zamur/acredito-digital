import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { Passo1aComponent } from './passo1a/passo1a.component';
import { Passo2Component } from './passo2/passo2.component';
import { Passo2aComponent } from './passo2a/passo2a.component';
import { Passo2bComponent } from './passo2b/passo2b.component';
import { Passo2cComponent } from './passo2c/passo2c.component';
import { Passo3aComponent } from './passo3a/passo3a.component';
import { Passo3bComponent } from './passo3b/passo3b.component';
import { Passo4aComponent } from './passo4a/passo4a.component';
import { Passo4bComponent } from './passo4b/passo4b.component';

const routes: Routes = [
    { path: '',  redirectTo: '/passo1', pathMatch: 'full'},
    { path: 'passo1', component: Passo1aComponent },
    { path: 'passo2', component: Passo2Component},
    { path: 'passo2a', component: Passo2aComponent},
    { path: 'passo2b', component: Passo2bComponent},
    { path: 'passo2c', component: Passo2cComponent},
    { path: 'passo3a', component: Passo3aComponent},
    { path: 'passo3b', component: Passo3bComponent},
    { path: 'passo4a', component: Passo4aComponent},
    { path: 'passo4b', component: Passo4bComponent},
];

export const routingModule: ModuleWithProviders = RouterModule.forRoot(routes);
