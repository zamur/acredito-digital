import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { routingModule } from './app.routing';

import { LayoutComponent } from './layout/layout.component';
import { Section0NavComponent } from './section0-nav/section0-nav.component';
import { Section0Nav1Component } from './section0-nav1/section0-nav1.component';
import { Passo1aComponent } from './passo1a/passo1a.component';
import { ModalComponent } from './passo1a/modal/modal.component';
import { Passo2Component } from './passo2/passo2.component';
import { Passo2aComponent } from './passo2a/passo2a.component';
import { Passo2bComponent } from './passo2b/passo2b.component';
import { Passo2cComponent } from './passo2c/passo2c.component';
import { Passo3aComponent } from './passo3a/passo3a.component';
import { Passo3bComponent } from './passo3b/passo3b.component';
import { Passo4aComponent } from './passo4a/passo4a.component';
import { Passo4bComponent } from './passo4b/passo4b.component';
import { Passo4aModalComponent } from './passo4a/passo4a-modal/passo4a-modal.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    routingModule
  ],
  declarations: [Section0NavComponent,
    LayoutComponent,
    Section0Nav1Component,
    Passo1aComponent,
    ModalComponent,
    Passo2Component,
    Passo2aComponent,
    Passo2bComponent,
    Passo2cComponent,
    Passo3aComponent,
    Passo3bComponent,
    Passo4aComponent,
    Passo4aModalComponent,
    Passo4bComponent
  ],
  entryComponents: [ModalComponent, Passo4aModalComponent],
  exports: [LayoutComponent]
})
export class FormModule { }
