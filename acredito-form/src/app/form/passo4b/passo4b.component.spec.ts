import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Passo4bComponent } from './passo4b.component';

describe('Passo4bComponent', () => {
  let component: Passo4bComponent;
  let fixture: ComponentFixture<Passo4bComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Passo4bComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Passo4bComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
