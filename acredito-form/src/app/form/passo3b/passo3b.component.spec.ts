import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Passo3bComponent } from './passo3b.component';

describe('Passo3bComponent', () => {
  let component: Passo3bComponent;
  let fixture: ComponentFixture<Passo3bComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Passo3bComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Passo3bComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
