import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Passo2cComponent } from './passo2c.component';

describe('Passo2cComponent', () => {
  let component: Passo2cComponent;
  let fixture: ComponentFixture<Passo2cComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Passo2cComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Passo2cComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
