import { Component, Renderer2, OnInit } from '@angular/core';

@Component({
  selector: 'app-section0-nav',
  templateUrl: './section0-nav.component.html',
  styleUrls: ['./section0-nav.component.scss']
})
export class Section0NavComponent implements OnInit {
  navbarOpen = false;
  selected = false;
  scroll;

  constructor(private renderer: Renderer2) { }

  ngOnInit() {
    this.renderer.listen(window, 'scroll', () => {
      this.scroll = window.scrollY;
   });
  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }
}
