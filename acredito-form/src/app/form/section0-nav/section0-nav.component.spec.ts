import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Section0NavComponent } from './section0-nav.component';

describe('Section0NavComponent', () => {
  let component: Section0NavComponent;
  let fixture: ComponentFixture<Section0NavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Section0NavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Section0NavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
