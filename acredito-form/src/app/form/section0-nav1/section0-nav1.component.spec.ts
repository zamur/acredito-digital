import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Section0Nav1Component } from './section0-nav1.component';

describe('Section0Nav1Component', () => {
  let component: Section0Nav1Component;
  let fixture: ComponentFixture<Section0Nav1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Section0Nav1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Section0Nav1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
