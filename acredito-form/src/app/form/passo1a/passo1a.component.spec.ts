import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Passo1aComponent } from './passo1a.component';

describe('Passo1aComponent', () => {
  let component: Passo1aComponent;
  let fixture: ComponentFixture<Passo1aComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Passo1aComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Passo1aComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
