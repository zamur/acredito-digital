import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from './modal/modal.component';

@Component({
  selector: 'app-passo1a',
  templateUrl: './passo1a.component.html',
  styleUrls: ['./passo1a.component.scss']
})

export class Passo1aComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  ngOnInit() {

  }

  open() {
    const modalRef = this.modalService.open(ModalComponent);
    // modalRef.componentInstance.name = 'World';
  }
}
